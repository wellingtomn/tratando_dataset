# TrabalhoAed

Esse projeto analisou dados de uma base onde foi feito a coleta de dados de varias familias do continente africano, sobre a agricultura familiar, e acabou sendo um dataset bem horizontal pela vasta quantidade de colunas que possui.


# Notação BIG O

## Complexidade do algoritmo: O(N^2) + O(log N) 

## Valores e testes

### Ordernação normal
    00:03:15
    00:03:11
    00:03:27

### Ordernação ordernada
    00:00:35
    00:00:35
    00:00:44

### Ordernação inversa
    00:00:03
    00:00:04
    00:00:04


### Busca com dados sem ordernar  
    00:00:27
    00:00:25
    00:00:26


### Busca com dados ordernada
    00:00:04
    00:00:03
    00:00:03

### Busca com dados ordernada e inversos
    00:00:08
    00:00:09
    00:00:09

## comandos no terminal 

### para rodar o projeto ***iex -S mix*** 

Com o modulo de `Teste` fica possivel fazer os teste de ações especificas da busca só com poucas linhas 
