defmodule TrabalhoAed.Teste do
  @moduledoc """
    Modulo Para fazer testes
  """

  @doc """
    Função para validar o tempo que demora para ordernar o caso normal
  """
  def init_normal() do
     TrabalhoAed.start_link()
  end

  @doc """
    Função para validar o tempo que demora para ordernar o pior caso
  """
  def init_desordenado() do
    GenServer.cast(:persistent_process, :order_reverse)
  end

  @doc """
    Função para validar o tempo que demora para ordernar o melhor caso
  """
  def init_ordenado() do
    GenServer.cast(:persistent_process, :order_order)
  end

  def request_melhor() do
    id = "10260830"

    TrabalhoAed.finded_safra(id, :order)
  end

  def request_medio() do
    id = "10260830"

    TrabalhoAed.finded_safra(id, :natural)
  end

  def request_pior() do
    id = "10260830"

    TrabalhoAed.finded_safra(id, :reverse)
  end

end
