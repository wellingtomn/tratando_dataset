defmodule TrabalhoAed.OrderFunctions do
  @moduledoc """
    Modulo ordernador de listas
  """

  @doc """
    Algoritmo de ordernação quick-sort fazendo a divisão e até a parte atomica da lista e fazendo a junções ordernada.
    Complexidade:
      pior caso: O(n^2)
      médio caso: O(n log n)
      melhor caso: O(n log n)

  """
  def qsort([]), do: []
  def qsort([h | t]) do
    {lesser, greater} = Enum.split_with(t, &(&1["nyieldc1"] < h["nyieldc1"]))
    qsort(lesser) ++ [h] ++ qsort(greater)
  end
end
