defmodule TrabalhoAed.Binary do
  @moduledoc """
    Modulo para fazer a busca Binaria dos dados
    pior caso: O(n log n)
    medio caso:  O(n log n)
    melhor caso:  O(1)

  """

  def search_binary(list, key, value) do
    {left, rigth}= Enum.split(list, 2)

    if length(rigth) == 1 do
      List.first(rigth)
    else
      case Enum.find(left, fn item -> Map.get(item, key) == value end) do
        nil -> search_binary(rigth, key, value)
        resp -> resp
      end
    end

  end
end
